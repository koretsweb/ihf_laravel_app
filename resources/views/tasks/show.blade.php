@extends('layouts.app')

@section('content')
    <div>
        <div style="margin: 20px">
            <a class="btn btn-primary" href="{{ route('tasks.create') }}">Добавить задачу</a>
        </div>
        <table class="table table-responsive">
            <th>Название</th>
            <th>Описание</th>
            <th>Статус</th>
            <th>Создан</th>
            <th>Обновлен</th>
            <tr>
                    <td>{{ $model->name }}</td>
                    <td>{{ $model->description }}</td>
                    <td>{{ $model->processed ? 'Выполнен' : 'Не выполнен'}}</td>
                    <td>{{ $model->created_at}}</td>
                    <td>{{ $model->updated_at}}</td>
            </tr>
        </table>
    </div>
@endsection
