@extends('layouts.app')

@section('content')
    <div>
        @if (session()->has('status'))
            <div class="alert-success alert">
                {{ session()->get('status') }}
            </div>
        @endif
    </div>
    <div>
        <div style="margin: 20px">
            <a class="btn btn-primary" href="{{ route('tasks.create') }}">Добавить задачу</a>
        </div>
        <table class="table table-responsive">
            <th>Название</th>
            <th>Описание</th>
            <th>Статус</th>
            <th>Действия</th>
            @foreach($models as $model)
                <tr style="{{ $model->processed ?: 'color: red' }}">
                    <td>{{ $model->name }}</td>
                    <td>{{ $model->description }}</td>
                    <td>{{ $model->processed ? 'Выполнен' : 'Не выполнен'}}</td>
                    <td>
                        <a href="{{ route('tasks.show', ['id' => $model->id]) }}">Посмотреть</a>
                        <a href="{{ route('tasks.delete', ['id' => $model->id]) }}">Удалить</a>
                        <a href="{{ route('tasks.mark', ['id' => $model->id]) }}">Сделано!</a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection
