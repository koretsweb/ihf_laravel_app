@extends('layouts.app')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
                {{ $error }}
            @endforeach
        </div>
    @endif
    <div class="container-fluid">
        <form action="{{ route('tasks.store_item') }}" method="POST">
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Name</label>
                <input type="text" id="name" class="form-control" name="name">

                <label for="description">Description</label>
                <input type="text" id="description" class="form-control" name="description">
            </div>

            <button type="submit" class="btn btn-success">Save</button>
        </form>
    </div>
@endsection
