<?php
/**
 * @author Dima Korets korets.web@gmail.com
 * @Date: 09.06.18
 */

namespace App\Http\Controllers;


use App\Model\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index()
    {
        $models = Task::all();

        return view('tasks.index', compact('models'));
    }

    public function create()
    {
        return view('tasks.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:20',
            'description' => 'required|string|max:200'
        ]);

        $model = new Task();

        $model->name = $request->get('name');
        $model->description = $request->get('description');

        $model->save();

        return redirect(route('tasks.index'))
            ->with('status', "Task {$model->name} has been added");
    }

    public function show($id, Request $request)
    {
        $model = Task::find($id);

        return view('tasks.show', compact('model'));
    }

    public function delete($id)
    {
        Task::destroy($id);

        return redirect(route('tasks.index'))
            ->with('status', "Task with id {$id} was successfully deleted");
    }

    public function mark($id)
    {
        $model = Task::find($id);

        $model->processed = true;
        $model->save();

        return redirect(route('tasks.index'))
            ->with('status', "Task with id {$id} was successfully done");
    }
}