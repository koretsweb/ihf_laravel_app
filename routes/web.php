<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/tasks', 'TaskController@index')->name('tasks.index');
Route::get('/tasks/show/{id}', 'TaskController@show')->name('tasks.show');
Route::get('/tasks/delete/{id}', 'TaskController@delete')->name('tasks.delete');
Route::post('/tasks', 'TaskController@store')->name('tasks.store_item');
Route::get('/tasks/create', 'TaskController@create')->name('tasks.create');
Route::get('/tasks/mark-as-processed/{id}', 'TaskController@mark')->name('tasks.mark');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
